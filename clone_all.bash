#!/bin/bash
#
#  clone all
#
#  clones in current working directory  VCS repositories
#  it is safe to run several times
#  so it can be used when an extra repository is needed
#
#  Tip: rerun as
#    1epic/clone_all.bash
#
while read DIR URL
do
   if [ ! -d $DIR ] ; then
     git clone $URL $DIR 
   else
     echo "I:Directory $DIR exists already"
   fi
done <  <( grep -v '^#' << HERE
# comment starts with hashtag
1epic https://salsa.debian.org/electronics-team/arduino/1epic.git
arduino https://salsa.debian.org/electronics-team/arduino/arduino.git
arduino-ignacio  https://github.com/HuayraLinux/pkg-arduino
go-errors-rock https://github.com/rockstorm101/go-errors-errors-debian-pkg
mrbean-ignacio https://github.com/HuayraLinux/pkg-jackson-module-mrbean
arduino https://salsa.debian.org/electronics-team/arduino/arduino.git
arduino-reference-en https://salsa.debian.org/electronics-team/arduino/reference-en.git
listserialportsc https://salsa.debian.org/electronics-team/arduino/listserialportsc.git
HERE
)

#
# and for the long run
while read PKG
do
   if [ ! -d $PKG ] ; then
      debcheckout $PKG
   else
      echo "I:debcheckout $PKG  already done"
   fi
done < <( grep -v '^#' << HERE
arduino
arduino-reference-en
listserialportsc 
HERE
)

# l l
